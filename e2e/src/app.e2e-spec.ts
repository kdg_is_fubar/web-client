import { AppPage } from './app.po';
import {$, $$, browser, by, element, protractor, ProtractorBrowser} from 'protractor';
import {create} from 'domain';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  // it('should display welcome message', () => {
  //   page.navigateTo();
  //   expect(page.getTitleText()).toEqual('Welcome to Front-End!');
  // });

  it('On successful login, there should be an usertoken', () => {
    browser.get('/login');

    element(by.css('[name="loginUsername"]')).sendKeys('appelmoes');
    element(by.css('[name="loginPassword"]')).sendKeys('appelmoes');
    element(by.css('[id="login"]')).click();

    const elementFromSecondPage = $('#intro_text');

    browser.wait(protractor.until.elementIsVisible(elementFromSecondPage.getWebElement()), 5000, 'Error: Element did not display within 5 seconds');
    const token = browser.executeScript('return window.localStorage.getItem(\'userToken\');');
    expect(token).toContain('Bearer');

  });

  it('add friend', () => {
    browser.get('/login');

    element(by.css('[name="loginUsername"]')).sendKeys('appelmoes');
    element(by.css('[name="loginPassword"]')).sendKeys('appelmoes');
    element(by.css('[id="login"]')).click();

    const elementFromSecondPage = $('#intro_text');
    browser.wait(protractor.until.elementIsVisible(elementFromSecondPage.getWebElement()), 5000, 'Error: Element did not display within 5 seconds');

    browser.get('/friends');

    element(by.css('[id="addFriendText"]')).sendKeys('sinaasappelsap');
    element(by.css('[id="addFriendButton"]')).click();

    browser.get('/profile');

    const logout = $('#logout');
    browser.wait(protractor.until.elementIsVisible(logout.getWebElement()), 5000, 'Error: Element did not display within 5 seconds');
    logout.click();

    browser.get('/login');

    element(by.css('[name="loginUsername"]')).sendKeys('sinaasappelsap');
    element(by.css('[name="loginPassword"]')).sendKeys('appelmoes');
    element(by.css('[id="login"]')).click();

    browser.wait(protractor.until.elementIsVisible(elementFromSecondPage.getWebElement()), 5000, 'Error: Element did not display within 5 seconds');

    browser.get('/friends');

    const listChange = $('#pending0');

    browser.wait(protractor.until.elementIsVisible(listChange.getWebElement()), 5000, 'Error: Element did not display within 5 seconds');

    // browser.pause();
    expect(listChange).toBeTruthy();
  });

  it('create room', () => {
    browser.get('/login');

    element(by.css('[name="loginUsername"]')).sendKeys('appelmoes');
    element(by.css('[name="loginPassword"]')).sendKeys('appelmoes');
    element(by.css('[id="login"]')).click();

    const elementFromSecondPage = $('#intro_text');
    browser.wait(protractor.until.elementIsVisible(elementFromSecondPage.getWebElement()), 5000, 'Error: Element did not display within 5 seconds');

    browser.get('/create_room');

    const crb = $('#createRoomButton');
    browser.wait(protractor.until.elementIsVisible(crb.getWebElement()), 5000, 'Error: Element did not display within 5 seconds');

    crb.click();
    const startGame = $('#startGame');
    browser.wait(protractor.until.elementIsVisible(startGame.getWebElement()), 5000, 'Error: Element did not display within 5 seconds');

    browser.get('/rooms');

    const room = $('#room0');
    browser.wait(protractor.until.elementLocated(room.locator()), 5000, 'aap');

    expect(room).toBeTruthy();
  });

  it('start game', () => {
    browser.get('/login');

    element(by.css('[name="loginUsername"]')).sendKeys('appelmoes');
    element(by.css('[name="loginPassword"]')).sendKeys('appelmoes');
    element(by.css('[id="login"]')).click();

    const elementFromSecondPage = $('#intro_text');
    browser.wait(protractor.until.elementIsVisible(elementFromSecondPage.getWebElement()), 5000, 'Error: Element did not display within 5 seconds');

    browser.get('/create_room');

    const crb = $('#createRoomButton');
    browser.wait(protractor.until.elementIsVisible(crb.getWebElement()), 5000, 'Error: Element did not display within 5 seconds');
    const slider = element(by.id('aiCount'));

    browser.actions().dragAndDrop(
      slider,
      {x: 100, y: 0}
    ).perform();

    crb.click();
    const startGame = $('#startGame');
    browser.wait(protractor.until.elementIsEnabled(startGame.getWebElement()), 5000, 'Error: Element did not display within 5 seconds');

    startGame.click();
    // const board = $('app-board-menu');
    // browser.wait(protractor.until.elementIsVisible(board.getWebElement()), 5000, 'Error: Element did not display within 5 seconds');
    browser.wait(protractor.until.urlContains('board'), 5000, 'error');

    expect(browser.getCurrentUrl()).toContain('board');
  });

});
