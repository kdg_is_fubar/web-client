import { Injectable } from '@angular/core';
import {Room} from '../../model/room';
import {JoinRoomDTO} from '../../model/join-room-dto';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {AuthenticationService} from '../../components/authentication/authenticationService';
import {NotifierService} from 'angular-notifier';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  private serverurl = environment.baseUrl + '/broker-guide';
  private stompClient;
  private readonly notifier: NotifierService;

  constructor(private http: HttpClient, private authenticationService: AuthenticationService, private notifierService: NotifierService) {
    this.notifier = notifierService;
  }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('userToken')
    })
  };

  setupConnection() {
    const socket = new SockJS(this.serverurl + '?token=' + localStorage.getItem('userToken'));
    this.stompClient = Stomp.over(socket);
    console.log(this.stompClient);
  }

  disconnect() {
    this.stompClient.disconnect(() => {
      console.log('user has been disconnected');
    });
  }

  getSocket() {
    return this.stompClient;
  }

  joinRoom(room: Room, username: string) {
    const joinroom = new JoinRoomDTO();
    joinroom.id = room.id;
    joinroom.username = username;
    joinroom.joining = true;
    this.stompClient.send('/rooms/join/' + room.id, this.httpOptions, JSON.stringify(joinroom));
  }
}
