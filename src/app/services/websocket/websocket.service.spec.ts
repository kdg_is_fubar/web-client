import { TestBed } from '@angular/core/testing';

import { WebsocketService } from './websocket.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NotifierModule} from "angular-notifier";

describe('WebsocketService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule,NotifierModule]
  }));

  it('should be created', () => {
    const service: WebsocketService = TestBed.get(WebsocketService);
    expect(service).toBeTruthy();
  });
});
