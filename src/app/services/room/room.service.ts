import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Tile} from '../../model/tile';
import {Room} from '../../model/room';
import {Observable} from 'rxjs';
import {JoinRoomDTO} from '../../model/join-room-dto';
import {TileDTO} from '../../model/tile-dto';
import {environment} from '../../../environments/environment';
import {AuthenticationService} from '../../components/authentication/authenticationService';
import {InviteStatus} from '../../model/InviteStatus';
import {Leaderboard} from '../../model/leaderboard';


@Injectable({
  providedIn: 'root'
})
export class RoomService {
  private stompClient;

  constructor(private http: HttpClient) {
  }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('userToken')
    })
  };

  public requestTileFromDeck(roomId: number): Promise<Tile> {
    return this.http.get(environment.baseUrl + '/room/' + roomId + '/draw', this.httpOptions)
      .toPromise()
      .then((resp: TileDTO) => this.parseTile(resp, false));
  }

  getTiles(roomId: number): Promise<Tile[]> {
    return this.http.get(environment.baseUrl + '/room/' + roomId + '/board', this.httpOptions)
      .toPromise()
      .then((resp: TileDTO[]) =>
        resp.map(tile =>
          this.parseTile(tile, true))
      );

  }

  private parseTile(tileDto: TileDTO, isPlaced): Tile {
    const tempTile = new Tile();
    tempTile.id = tileDto.id;
    tempTile.rotations = tileDto.rotations;
    tempTile.name = tileDto.name;
    tempTile.startTile = tileDto.startTile;
    tempTile.x = tileDto.x;
    tempTile.y = tileDto.y;
    tempTile.isPlaced = isPlaced;
    return tempTile;
  }

  createRoom(name: string, maxPlayerCount: number, username: string, aiCount: number, turnTime: number) {
    return this.http.post(environment.baseUrl + '/room', {
      name: name,
      maxPlayerCount: maxPlayerCount,
      username: username,
      aiCount: aiCount,
      turnTime: turnTime
    }, this.httpOptions)
      .toPromise();
  }

  getRooms(): Observable<Room[]> {
    return this.http.get<Room[]>(environment.baseUrl + '/room/all', this.httpOptions);
  }

  getRoom(id): any {
    return this.http.get(environment.baseUrl + '/room/' + id, this.httpOptions);
  }

  joinRoom(room: Room, username: string) {
    const joinroom = new JoinRoomDTO();
    joinroom.id = room.id;
    joinroom.username = username;
    joinroom.joining = true;
    this.stompClient.send('/rooms/join/' + room.id, this.httpOptions, JSON.stringify(joinroom));
  }

  checkMyTurn(roomId: number): Observable<Boolean> {
    return this.http.get<Boolean>(environment.baseUrl + '/room/' + roomId + '/checkturn', this.httpOptions);
  }
}
