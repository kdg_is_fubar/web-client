import { Injectable } from '@angular/core';
import {Room} from '../../model/room';
import {Observable} from 'rxjs';
import {InviteStatus} from '../../model/InviteStatus';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InvitationService {

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('userToken')
    })
  };

  constructor(private http: HttpClient) { }

  inviteRoom(room: Room, username: string): Observable<InviteStatus> {
    return this.http.get<InviteStatus>(environment.baseUrl + '/invite/' + room.id + '/' + username, this.httpOptions);
  }

  inviteEmailRoom(room: Room, email: string): Observable<InviteStatus> {
    return this.http.get<InviteStatus>(environment.baseUrl + '/invite/' + room.id + '/email/' + email, this.httpOptions);
  }
}
