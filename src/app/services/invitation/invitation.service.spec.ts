import { TestBed } from '@angular/core/testing';

import { InvitationService } from './invitation.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('InvitationService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [HttpClientTestingModule]}));

  it('should be created', () => {
    const service: InvitationService = TestBed.get(InvitationService);
    expect(service).toBeTruthy();
  });
});
