import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Tile} from '../../model/tile';
import {environment} from '../../../environments/environment';
import {TileDTO} from '../../model/tile-dto';
import {Observable} from 'rxjs';
import {Room} from '../../model/room';
import {JoinRoomDTO} from '../../model/join-room-dto';
import {SurfaceType} from '../../model/surface-type.enum';
import {AuthenticationService} from '../../components/authentication/authenticationService';
import {TileService} from '../tile/tile.service';
import {Leaderboard} from '../../model/leaderboard';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
  }

  private httpOptions(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('userToken')
    });
  }

  public requestTileFromDeck(roomId: number): Promise<Tile> {
    return this.http.get(environment.baseUrl + '/room/' + roomId + '/draw', {headers: this.httpOptions()})
      .toPromise()
      .then((resp: TileDTO) => TileService.parseTile(resp, false)).catch(err => {
        return Promise.reject(err.error || 'Server error');
      });
  }

  getBoard(roomId: number): Promise<Tile[]> {
    return this.http.get(environment.baseUrl + '/game/' + roomId + '/board', {headers: this.httpOptions()})
      .toPromise()
      .then((resp: TileDTO[]) =>
        resp.map(tile =>
          TileService.parseTile(tile, true))
      ).catch(err => {
        return Promise.reject(err.error || 'Server error');
      });
  }

  public requestTilePlacement(roomId: number, tile: Tile, x: number, y: number): Promise<boolean> {
    const tileDTO = new TileDTO(); // TODO use this DTO
    return this.http.post(
      environment.baseUrl + '/game/' + roomId + '/placetile', {
        x: x,
        y: -y, // y axis front end and backend are mirrored
        startTile: tile.startTile,
        name: tile.name,
        rotations: tile.rotations
      }, {headers: this.httpOptions(), observe: 'response'})
      .toPromise().then((resp) => {
        return resp.body === true;
      });
  }

  public verifyFollower(roomId: number,  tileX: number, tileY: number, partX: number, partY: number): Promise<SurfaceType> {
    return this.http.post<SurfaceType>(environment.baseUrl + '/game/' + roomId + '/verifyfollower', {
        relativeX: partX,
        relativeY: partY,
        placed: false
      },
      {
        headers: this.httpOptions(),
        observe: 'response'
      }).toPromise().then((resp) => resp.body);

  }

  public placeFollower(roomId: number, tileX: number, tileY: number, partX: number, partY: number) {
    return this.http.post<SurfaceType>(environment.baseUrl + '/game/' + roomId + '/placefollower', {
        relativeX: partX,
        relativeY: partY,
        placed: false
      },
      {
        headers: this.httpOptions(),
        observe: 'response'
      }).toPromise().then((resp) => resp.body);
  }

  checkMyTurn(roomId: number): Observable<Boolean> {
    return this.http.get<Boolean>(environment.baseUrl + '/game/' + roomId + '/checkturn', {headers: this.httpOptions()});
  }

  endTurn(roomId: number) {
    return this.http.get<Boolean>(environment.baseUrl + '/game/' + roomId + '/endturn', {headers: this.httpOptions()}).subscribe();
  }

  getLeaderboard(roomId: number): Observable<Leaderboard> {
    return this.http.get<Leaderboard>(environment.baseUrl + '/game/' + roomId + '/leaderboard', {headers: this.httpOptions()});
  }
}
