import { TestBed } from '@angular/core/testing';

import { NotificationService } from './notification.service';
import {HttpClientModule} from "@angular/common/http";
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('HistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports:[HttpClientTestingModule]}));

  it('should be created', () => {
    const service: NotificationService = TestBed.get(NotificationService);
    expect(service).toBeTruthy();
  });
});
