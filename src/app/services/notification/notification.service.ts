import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {NotificationDTO} from '../../model/notification-dto';
import {BehaviorSubject, Observable} from 'rxjs';
import {take} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private http: HttpClient) {
  }


  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('userToken')
    })
  };

  getNotifications(): Observable<NotificationDTO[]> {
    return this.http.get<NotificationDTO[]>(environment.baseUrl + '/notification', this.httpOptions);
  }
  delete(id: number) {
    return this.http.get(environment.baseUrl + '/notification/delete/' + id, this.httpOptions);
  }

  // addPushSubscriber(sub: any) {
  //   console.log(sub);
  //   return this.http.post('https://fubar-backend.herokuapp.com/api/notifications', sub);
  // }
  //
  // send() {
  //   return this.http.post('https://fubar-backend.herokuapp.com/api/newsletter', null);
  // }
}
