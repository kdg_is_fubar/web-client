import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {Replay} from "../../model/replay";
import {Tile} from "../../model/tile";
import {TileDTO} from "../../model/tile-dto";


@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  constructor(private http: HttpClient) {
  }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('userToken')
    })
  };


  getReplays(): Observable<Replay[]> {
    return this.http.get<Replay[]>(environment.baseUrl + '/replay/all', this.httpOptions);
  }

  getReplay(id:string): Observable<Replay> {
    return this.http.get<Replay>(environment.baseUrl + '/replay/'+id, this.httpOptions);
  }

  getNext(id:number): Observable<TileDTO[]> {
      return this.http.get<TileDTO[]>(environment.baseUrl + '/replay/'+id+'/next', this.httpOptions);

  }

  reset(replayId: number): Observable<TileDTO[]> {
    return this.http.get<TileDTO[]>(environment.baseUrl + '/replay/'+replayId+'/reset', this.httpOptions);
  }
}
