import { TestBed } from '@angular/core/testing';

import { CanvasService } from './canvas.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('CanvasService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [HttpClientTestingModule]}));

  it('should be created', () => {
    const service: CanvasService = TestBed.get(CanvasService);
    expect(service).toBeTruthy();
  });
});
