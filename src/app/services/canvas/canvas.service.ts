import {AfterViewInit, Injectable, OnInit} from '@angular/core';
import {Tile} from '../../model/tile';
import {TileService} from '../tile/tile.service';
import {AuthenticationService} from '../../components/authentication/authenticationService';
import {Follower} from '../../model/follower';
import {TileDTO} from '../../model/tile-dto';
import {BoardStatus} from '../../model/board-status.enum';
import {RoomService} from '../room/room.service';
import {GameService} from '../game/game.service';

@Injectable({
  providedIn: 'root'
})
export class CanvasService {

  canvas;
  ctx;
  zoom = 150;
  centerX = 0;
  centerY = 0;
  cursorX: number;
  cursorY: number;

  // region tiles
  private tilesPromise: Promise<Tile[]>;
  tiles: Tile[];
  holdingTile = false;
  toPlaceTile;
  lastplaceX;
  lastplaceY;
  lastPlacedTile: Tile;
  // endregion
  // region meeples
  holdingMeeple = false;
  private myMeeple: HTMLImageElement;
  showPickMeeple: boolean;
  roomId: number;
  boardStatus = BoardStatus.AWAITING_TURN;
  ended = false;


  public surfaceType = '';
  public tileX: number;
  public tileY: number;
  public partX: number;
  public partY: number;
  public verifying: boolean;

  // endregion

  constructor(private gameService: GameService,
              private tileService: TileService,
              private authenticationService: AuthenticationService,
              private roomService: RoomService) {
    this.myMeeple = new Image();
    this.myMeeple.src = '../../../assets/img/meeple.png';
  }


  initGame(id: number, username: string) {
    const currentUser = this.authenticationService.currentUserValue;
    if (this.boardStatus === BoardStatus.REPLAY) {

    } else {
      if (currentUser) {
        this.roomId = id;
        this.tilesPromise = this.gameService.getBoard(id);
        this.roomService.getRoom(id).subscribe(value => {
          this.ended = value.hasEnded;
          if (value.players.find(x => x.username === username).drawnTile !== null) {
            this.placeTileInHand(value.players.find(x => x.username === username).drawnTile);
            this.boardStatus = BoardStatus.PLACING_TILE;
          }
        });
        this.tilesPromise
          .then(value => {
            this.tiles = value;
            this.loadTiles();
          }).catch(err => {
          return Promise.reject(err.error || 'Server error');
        });
      }
    }

  }

  updateTiles(updatedTiles: TileDTO[]) {
    this.tiles = updatedTiles.map(tile =>
      TileService.parseTile(tile, true));
    this.loadTiles();
  }

  private loadTiles() {
    if (this.tiles) {
      this.tiles.forEach(tile => {
        tile.image = new Image();
        tile.image.src = '../../../assets/img/r' + tile.rotations + '/' + tile.name + '.jpg';
        tile.image.onload = () => {
          this.drawTile(tile, this.ctx, this.zoom);
          // this.myMeeple.onload = () => {
          if (tile.follower != null) {
            this.drawMeeple(tile);
          }
          // };
        };

      });
    }
    this.redrawCanvas();
  }

  endTurn() {
    this.holdingMeeple = false;
    this.showPickMeeple = false;
    this.boardStatus = BoardStatus.AWAITING_TURN;
  }

  getMousePos(canvas, evt) {
    const rect = canvas.getBoundingClientRect();
    return {
      x: (evt.clientX - rect.left) / (rect.right - rect.left) * canvas.width,
      y: (evt.clientY - rect.top) / (rect.bottom - rect.top) * canvas.height
    };
  }

  // region drawing
  redrawCanvas() {
    if (this.ended) {
      return;
    }
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    if (this.tiles) {
      this.tiles.forEach(tile => {
        this.drawTile(tile, this.ctx, this.zoom);

      });
      this.tiles.forEach(tile => {
        if (tile.follower != null) {
          this.drawMeeple(tile);
        }
      });
      if (this.holdingMeeple) {
        this.drawMeeple();
      }
    }
    if (this.toPlaceTile) {
      this.drawTile(this.toPlaceTile, this.ctx, this.zoom);
    }
  }

  // endregion

  // region tile
  private drawTile(tile: Tile, ctx, zoom) {
    if (this.ended) {
      return;
    }
    let x: number;
    let y: number;
    if (tile) {
      if (tile.isPlaced) {
        x = tile.x * zoom + this.centerX - this.zoom / 2;
        y = tile.y * zoom + this.centerY - this.zoom / 2;
        ctx.drawImage(tile.image, x, y, zoom, zoom);
      } else {
        if (this.toPlaceTile && !this.holdingTile) {
          this.toPlaceTile.x = this.canvas.width / 2 - this.zoom / 2;
          this.toPlaceTile.y = this.canvas.height - this.zoom * 2;
        }
        x = this.toPlaceTile.x + zoom * 0.025;
        y = this.toPlaceTile.y + zoom * 0.025;
        const dimension = zoom / 10 * 9.5;
        ctx.strokeStyle = '#fff600';
        ctx.lineWidth = 3;
        this.ctx.drawImage(this.toPlaceTile.image, x, y, dimension, dimension);
        ctx.strokeRect(x, y, dimension, dimension);
      }
    }
  }

  placeTileInHand(value) {
    value.image = new Image();
    value.image.src = '../../../assets/img/r' + value.rotations + '/' + value.name + '.jpg';
    this.holdingTile = false;
    if (!value.isPlaced) {
      this.toPlaceTile = value;
    }
    value.image.onload = () => this.redrawCanvas();
  }

  putDownTile(x: number, y: number) {
    this.toPlaceTile.isPlaced = true;
    this.lastPlacedTile = this.toPlaceTile;
    this.toPlaceTile.x = x;
    this.toPlaceTile.y = y;
    this.tiles.push(this.toPlaceTile);
    this.toPlaceTile = undefined;
    this.holdingTile = false;
    this.showPickMeeple = true;
    this.boardStatus = BoardStatus.PLACED_TILE;
    this.redrawCanvas();
  }

  pickTile() {
    if (this.ended) {
      return;
    }
    this.lastplaceX = this.getMousePos(this.canvas, event).x;
    this.lastplaceY = this.getMousePos(this.canvas, event).y;

    if (this.toPlaceTile) {
      this.turnToPlaceTile();
    }
  }

  turnToPlaceTile() {
    if (this.toPlaceTile) {
      this.toPlaceTile.rotations = (this.toPlaceTile.rotations + 1) % 4;
      this.toPlaceTile.image.src = '../../../assets/img/r' + this.toPlaceTile.rotations + '/' + this.toPlaceTile.name + '.jpg';
      this.redrawCanvas();
    }
  }


  // endregion

  // region follower
  private drawMeeple(tile?: Tile) {

    if (this.ended) {
      return;
    }
    let x, y;
    if (tile != null && tile.follower.placed) {
      x = tile.x * this.zoom + this.centerX - this.zoom / 2;
      y = tile.y * this.zoom + this.centerY - this.zoom / 2;
      x += this.zoom / 9 * tile.follower.relativeX - (this.zoom / 9) * 1.5;
      y += this.zoom / 9 * tile.follower.relativeY - (this.zoom / 9) * 3;
      this.ctx.drawImage(this.myMeeple, x, y, this.zoom / 3, this.zoom / 3);
    } else if (this.holdingMeeple) {
      x = this.cursorX - (this.zoom / 9) * 1.5;
      y = this.cursorY - (this.zoom / 9) * 3;
      this.ctx.drawImage(this.myMeeple, x, y, this.zoom / 3, this.zoom / 3);
    }
  }

  pickMeeple() {
    if (this.ended) {
      return;
    }
    this.boardStatus = BoardStatus.PLACING_FOLLOWER;
    this.holdingMeeple = true;
  }

  tryPlaceMeeple(event) {
    const x = this.lastPlacedTile.x * this.zoom + this.centerX - this.zoom / 2;
    const y = this.lastPlacedTile.y * this.zoom + this.centerY - this.zoom / 2;
    const x_max = (this.lastPlacedTile.x + 1) * this.zoom + this.centerX - this.zoom / 2;
    const y_max = (this.lastPlacedTile.y + 1) * this.zoom + this.centerY - this.zoom / 2;

    if (!this.verifying) {
      const xFactor = (event.layerX - this.centerX) / this.zoom;
      const yFactor = (event.layerY - this.centerY) / this.zoom;
      if (event.layerX >= x && event.layerX <= x_max
        && event.layerY >= y && event.layerY <= y_max) {

        const tileX = Math.round(xFactor);
        const tileY = Math.round(yFactor);
        const partX = Math.round((xFactor + 0.5 - Math.round(xFactor)) * 8);
        const partY = Math.round((yFactor + 0.5 - Math.round(yFactor)) * 8);
        this.gameService.verifyFollower(this.roomId, tileX, tileY, partX, partY).then(surfaceType => {
          // TODO Clean confirm
          if (surfaceType != null) {
            this.showVerification(surfaceType, tileX, tileY, partX, partY);
          }
        });
      }

    }
  }

  showVerification(surfaceType, tileX, tileY, partX, partY) {
    this.verifying = true;
    this.surfaceType = surfaceType;
    this.tileX = tileX;
    this.tileY = tileY;
    this.partX = partX;
    this.partY = partY;
  }


  placeFollower(tileX: number, tileY: number, partX: number, partY: number) {
    this.gameService.placeFollower(this.roomId, tileX, tileY, partX, partY);
    const follower = new Follower();
    follower.relativeX = partX;
    follower.relativeY = partY;
    follower.placed = true;
    this.tiles.find(tile => tile.x === tileX && tile.y === tileY).follower = follower;
    this.holdingMeeple = false;
    this.showPickMeeple = false;
    this.redrawCanvas();
    this.gameService.endTurn(this.roomId);
  }

  // endregion
}
