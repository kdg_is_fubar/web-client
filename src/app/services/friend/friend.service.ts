import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {FriendDTO} from '../../model/friendDTO';

@Injectable({
  providedIn: 'root'
})
export class FriendService {

  constructor(private http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('userToken')
    })
  };

  getFriends() {
    return this.http.get(environment.baseUrl + '/friend/getall', this.httpOptions);
  }

  addFriend(friendDTO: FriendDTO) {
    return this.http.post(environment.baseUrl + '/friend/add', JSON.stringify(friendDTO), this.httpOptions);
  }

  deleteFriend(friendDTO: FriendDTO) {
    return this.http.post(environment.baseUrl + '/friend/delete', JSON.stringify(friendDTO), this.httpOptions);
  }

  acceptInvite(friendDTO: FriendDTO) {
    return this.http.post(environment.baseUrl + '/friend/accept', JSON.stringify(friendDTO), this.httpOptions);

  }

  refuseInvite(friendDTO: FriendDTO) {
    return this.http.post(environment.baseUrl + '/friend/refuse', JSON.stringify(friendDTO), this.httpOptions);
  }
}
