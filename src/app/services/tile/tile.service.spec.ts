import { TestBed } from '@angular/core/testing';

import { TileService } from './tile.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('TileService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [HttpClientTestingModule]}));

  it('should be created', () => {
    const service: TileService = TestBed.get(TileService);
    expect(service).toBeTruthy();
  });
});
