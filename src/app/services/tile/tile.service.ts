import {Injectable} from '@angular/core';
import {Tile} from '../../model/tile';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TileDTO} from '../../model/tile-dto';
import {AuthenticationService} from '../../components/authentication/authenticationService';
import {environment} from '../../../environments/environment';
import {SurfaceType} from '../../model/surface-type.enum';


@Injectable({
  providedIn: 'root'
})
export class TileService {
  static parseTile(tileDto: TileDTO, isPlaced): Tile {
    const tempTile = new Tile();
    tempTile.id = tileDto.id;
    tempTile.rotations = tileDto.rotations;
    tempTile.name = tileDto.name;
    tempTile.startTile = tileDto.startTile;
    tempTile.x = tileDto.x;
    tempTile.y = -tileDto.y;
    tempTile.isPlaced = isPlaced;
    tempTile.follower = tileDto.follower;
    return tempTile;
  }
}
