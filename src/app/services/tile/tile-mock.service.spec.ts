import { TestBed } from '@angular/core/testing';

import { TileMockService } from './tile-mock.service';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('TileMockService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [HttpClientTestingModule]}));

  it('should be created', () => {
    const service: TileMockService = TestBed.get(TileMockService);
    expect(service).toBeTruthy();
  });
});
