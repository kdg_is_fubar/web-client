import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthenticationService} from '../../components/authentication/authenticationService';
import {Tile} from '../../model/tile';
import {Follower} from '../../model/follower';
import {SurfaceType} from '../../model/surface-type.enum';

@Injectable({
  providedIn: 'root'
})
export class TileMockService {
  private board = Array();

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
    const tile = new Tile();
    tile.follower = new Follower();
    tile.follower.relativeX = 5;
    tile.follower.relativeY = 3;
    tile.rotations = 0;
    tile.isPlaced = true;
    tile.x = 0;
    tile.y = 0;
    tile.startTile = true;
    tile.name = 'g';
    this.board.push(tile);
  }

  public requestTileFromDeck(roomId: number): Promise<Tile> {
    const tile = new Tile();
    tile.follower = null;
    tile.rotations = 0;
    tile.isPlaced = false;
    tile.x = 0;
    tile.y = 0;
    tile.startTile = false;
    tile.name = 'a';
    return Promise.resolve(tile);
  }


  getTiles(roomId: number): Promise<Tile[]> {
    return Promise.resolve(this.board);
  }

  public requestTilePlacement(roomId: number, tile: Tile): Promise<boolean> {
    this.board.push(tile);
    return Promise.resolve(true);
  }

  public verifyFollower(roomId: number, tileX: number, tileY: number, partX: number, partY: number): Promise<SurfaceType> {
    return Promise.resolve(SurfaceType.FIELD);
  }

  public placeFollower(roomId: number, tileX: number, tileY: number, partX: number, partY: number) {

  }
}
