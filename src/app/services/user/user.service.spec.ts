import {inject, TestBed} from '@angular/core/testing';

import { UserService } from './user.service';
import {MatButtonModule, MatFormFieldModule, MatInputModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('UserService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [UserService],
    imports: [HttpClientTestingModule]


  }));

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  // TODO: delete if it doesn't work

  // it('should get a 200 response on successful registration', () => {
  //
  //   // setTimeout(() => {
  //     inject([UserService], (userService: UserService) => {
  //       userService.registerUser('someRegisteredUser', 'somePassword').subscribe((resp: Response) => {
  //           expect(resp.headers.get('Authentication')).toBe(null);
  //         });
  //     });
  //
  //
  //   // }, 2000);
  //
  // });
  //
  // it('should get an Authorization header + JWT', () => {
  //   const service: UserService = TestBed.get(UserService);
  //   inject([UserService], (userService: UserService) => {
  //     expect(userService.getUser('someRegisteredUser', 'somePassword')).toContain('test');
  //     });
  //   });
});
