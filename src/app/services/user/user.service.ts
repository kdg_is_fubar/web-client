import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {setHostBindings} from '@angular/core/src/render3/instructions';
import {environment} from '../../../environments/environment';
import {reject} from 'q';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private JWTResponseToken: string;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('userToken')
    })
  };

  constructor(private http: HttpClient) {
  }

  /**
   * "getUser" will dispatch an log in API call to the back end to ask for a response.
   * This response will return a 401 if the user does not get authenticated (wrong data provided or account does not exist)
   * Or a 302 which is a redirect, this will not be acted upon. Instead we will make our own redirection in Angular itself.
   */
  public getUser(username: string, password: string): any {
    return this.http.post(environment.baseUrl + '/login', {
      username: username,
      password: password
    }, {observe: 'response'});
  }

  public getProfile(): Promise<any> {
    return new Promise((resolve) => {
      this.http.get(environment.baseUrl + '/user/api/profile', this.httpOptions)
        .subscribe(data => {
          resolve(data);
        });
    });
  }

  public deleteUser(): Promise<number> {
    return new Promise((resolve) => {
      this.http.get(environment.baseUrl + '/user/api/delete', this.httpOptions)
        .subscribe(data => {
          resolve(Number(data));
        });
    });
  }

  public sendReset(email: string): any {
    return this.http.post(environment.baseUrl + '/user/api/resetPassword', {
      email
    }).subscribe();
  }

  public registerUser(username: string, email: string, password: string): any {
    return this.http.post(environment.baseUrl + '/user/api/register', {
      username: username,
      email: email,
      password: password
    }, {observe: 'response'});
  }

  verifyToken(username: string, token: string) {
    this.http.post(environment.baseUrl + '/user/api/verifyToken', {
      username: username,
      verificationToken: token
    }).subscribe(null, error => console.log('oops', error));
  }

  getVisitorProfile(username): Promise<any> {
    return new Promise((resolve) => {
      this.http.get(environment.baseUrl + '/user/api/visit/' + username, this.httpOptions)
        .subscribe(data => {
          resolve(data);
        });
    });
  }

  changeAvailability() {
    return this.http.post(environment.baseUrl + '/user/api/changeStatsStatus', {}, this.httpOptions);
  }

  changePassword(email: string, pass: string) {
    return this.http.post(environment.baseUrl + '/user/api/changePassword', {
      email: email,
      password: pass
    });
  }
}
