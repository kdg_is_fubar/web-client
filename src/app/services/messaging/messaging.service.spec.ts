import {TestBed} from '@angular/core/testing';

import {MessagingService} from './messaging.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {AngularFireDatabase, AngularFireDatabaseModule} from "@angular/fire/database";
import {AngularFireModule} from "@angular/fire";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {AngularFireMessagingModule} from "@angular/fire/messaging";
import {NotifierModule} from "angular-notifier";

describe('MessagingService', () => {
  const fbConfig = {
    apiKey: 'AIzaSyAQQtiHkyRwYMOUZ6NKZIRIycmjYiQabtI',
    authDomain: 'carcasonne.firebaseapp.com',
    databaseURL: 'https://carcasonne.firebaseio.com/',
    projectId: 'carcasonne',
    storageBucket: 'carcasonne.appspot.com',
    messagingSenderId: '361925331015'
  };

  beforeEach(() => TestBed.configureTestingModule({
    providers: [MessagingService],
    imports: [HttpClientTestingModule, AngularFireModule.initializeApp(fbConfig), AngularFireDatabaseModule, AngularFireAuthModule, AngularFireMessagingModule, NotifierModule]
  }));

  it('should be created', () => {
    const service: MessagingService = TestBed.get(MessagingService);
    expect(service).toBeTruthy();
  });
});
