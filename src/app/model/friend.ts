export class Friend {
  id: number;
  username: string;
  pending: boolean;
}
