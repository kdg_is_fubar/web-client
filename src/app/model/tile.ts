import {Follower} from './follower';

export class Tile {
  id: number;
  x: number;
  y: number;
  rotations: number;
  startTile: boolean;
  name: string;
  image: HTMLImageElement;
  isPlaced: boolean;
  follower?: Follower;
}
