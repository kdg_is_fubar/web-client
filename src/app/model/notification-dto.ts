export class NotificationDTO {
  id: number;
  message: string;
  roomId: number;
  roomName: string;
  username: string;
  isStarted: boolean;
}
