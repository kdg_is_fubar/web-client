export class Follower {
  placed: boolean;
  relativeX: number;
  relativeY: number;
  playerId: number;
}
