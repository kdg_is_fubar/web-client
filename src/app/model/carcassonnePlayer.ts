export class carcassonnePlayer {
  username: string;
  email: string;
  password: string;
  times_played;
  times_won;
  average_score;
  maximum_score;
  statsPublic: boolean;
}
