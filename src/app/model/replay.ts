export class Replay {
  id: number;
  public roomName: string;
  public userNames: string[];
  public endScores: number[];
}
