import {carcassonnePlayer} from './carcassonnePlayer';

export class Leaderboard {
  playerUsernames: string[];
  scores: number[];
}
