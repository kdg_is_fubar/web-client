export class Room {
  private _id: number;
  private _name: String;
  private _maxPlayerCount: number;
  private _username: string;
  private _aiCount: number;
  private _players;
  private _currentPlayerId: number;

  constructor(id: number, name: String, maxPlayerCount: number, username: string, aiCount: number) {
    this._id = id;
    this._name = name;
    this._maxPlayerCount = maxPlayerCount;
    this._username = username;
    this._aiCount = aiCount;
    this.players = [];
  }


  get id(): number {
    return this._id;
  }
  set id(value: number) {
    this._id = value;
  }
  set currentPlayerId(value: number) {
    this._currentPlayerId = value;
  }
  get currentPlayerId(): number {
    return this._currentPlayerId;
  }



  get name(): String {
    return this._name;
  }

  set name(value: String) {
    this._name = value;
  }
  get players() {
    return this._players;
  }

  set players(value) {
    this._players = value;
  }

  get maxPlayerCount(): number {
    return this._maxPlayerCount;
  }

  set maxPlayerCount(value: number) {
    this._maxPlayerCount = value;
  }

  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }

  get aiCount(): number {
    return this._aiCount;
  }

  set aiCount(value: number) {
    this._aiCount = value;
  }
}
