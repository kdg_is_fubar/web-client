export class FriendDTO {
  currentUsername: string;
  friendUsername: string;
}
