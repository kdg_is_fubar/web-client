import {Follower} from './follower';

export class TileDTO {
  id: number;
  x: number;
  y: number;
  rotations: number;
  startTile: boolean;
  name: string;
  follower: Follower;
}
