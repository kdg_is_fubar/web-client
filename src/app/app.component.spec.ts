import {TestBed, async} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {MenuComponent} from "./components/menu/menu.component";
import {NotifierModule} from "angular-notifier";
import {MenuItemComponent} from "./components/menu-item/menu-item.component";
import {RouterTestingModule} from "@angular/router/testing";
import {MessagingService} from "./services/messaging/messaging.service";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {AngularFireModule} from "@angular/fire";
import {AngularFireDatabaseModule} from "@angular/fire/database";
import {environment} from "../environments/environment";
import {ServiceWorkerModule} from "@angular/service-worker";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {AngularFireMessagingModule} from "@angular/fire/messaging";

const fbConfig = {
  apiKey: 'AIzaSyAQQtiHkyRwYMOUZ6NKZIRIycmjYiQabtI',
  authDomain: 'carcasonne.firebaseapp.com',
  databaseURL: 'https://carcasonne.firebaseio.com/',
  projectId: 'carcasonne',
  storageBucket: 'carcasonne.appspot.com',
  messagingSenderId: '361925331015'
};

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [MessagingService],
      declarations: [
        AppComponent, MenuComponent, MenuItemComponent
      ],
      imports: [RouterTestingModule, NotifierModule, HttpClientTestingModule, AngularFireModule.initializeApp(fbConfig), AngularFireDatabaseModule, AngularFireAuthModule, AngularFireMessagingModule, NotifierModule,
        ServiceWorkerModule.register('src/firebase-messaging-sw.js', {enabled: environment.production}),
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });


});
