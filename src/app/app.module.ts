import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {MenuComponent} from './components/menu/menu.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {HomeComponent} from './components/home/home.component';
import {RulesComponent} from './components/rules/rules.component';
import {RoomsComponent} from './components/rooms/rooms.component';
import {NotificationsComponent} from './components/notifications/notifications.component';
import {FriendsComponent} from './components/friends/friends.component';
import {LoginComponent} from './components/login/login.component';
import {ProfileComponent} from './components/profile/profile.component';
import {RoomComponent} from './components/room/room.component';
import {RoomCreateComponent} from './components/room-create/room-create.component';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RegisterComponent} from './components/register/register.component';
import {AuthGuard} from './components/authentication/auth_guard';
import {BoardComponent} from './components/board/board.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {HttpClientModule} from '@angular/common/http';
import {
  AuthServiceConfig,
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialLoginModule,
} from 'angular-6-social-login-v2';
import {VerificationComponent} from './components/verification/verification.component';
import {MailComponent} from './components/mail/mail.component';
import {MenuItemComponent} from './components/menu-item/menu-item.component';
import {PlayerComponent} from './components/player/player.component';
import {ChatComponent} from './components/chat/chat.component';
import {RoomItemComponent} from './components/room-item/room-item.component';
import {NotificationItemComponent} from './components/notification-item/notification-item.component';
import {NotifierModule, NotifierOptions} from 'angular-notifier';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireMessagingModule} from '@angular/fire/messaging';
import {AngularFireModule} from '@angular/fire';
import {AsyncPipe} from '@angular/common';
import {MessagingService} from './services/messaging/messaging.service';
import {BoardMenuComponent} from './components/board-menu/board-menu.component';
import {HistoryComponent} from './components/history/history.component';
import {HistoryItemComponent} from './components/history-item/history-item.component';
import { EndGameComponent } from './components/end-game/end-game.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';

const appRoutes: Routes = [
  {path: 'rules', component: RulesComponent},
  {path: 'rooms', component: RoomsComponent},
  {path: 'board/:id', component: BoardComponent},
  {path: 'create_room', component: RoomCreateComponent, canActivate: [AuthGuard]},
  {path: 'notifications', component: NotificationsComponent, canActivate: [AuthGuard]},
  {path: 'friends', component: FriendsComponent, canActivate: [AuthGuard]},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'mail', component: MailComponent},
  {path: 'changePassword', component: ChangePasswordComponent},
  {path: 'resetPassword', component: ResetPasswordComponent},
  {path: 'history', component: HistoryComponent, canActivate: [AuthGuard]},
  {path: ':username&:verificationToken', component: HomeComponent},
  {path: 'room/:id', component: RoomComponent, canActivate: [AuthGuard]},
  {path: 'profile/:username', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'replay/:id', component: BoardComponent, canActivate: [AuthGuard]},
  {path: '', component: HomeComponent},
  {path: '**', component: PageNotFoundComponent},
];

export function getMockedConfig() {
  const config = new AuthServiceConfig(
    []
  );
  return config;
}

const fbConfig = {
  apiKey: 'AIzaSyAQQtiHkyRwYMOUZ6NKZIRIycmjYiQabtI',
  authDomain: 'carcasonne.firebaseapp.com',
  databaseURL: 'https://carcasonne.firebaseio.com',
  projectId: 'carcasonne',
  storageBucket: 'carcasonne.appspot.com',
  messagingSenderId: '361925331015'
};

export function getAuthServiceConfigs() {
  const config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('1020554894803914')
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider('361925331015-jbvu4mlucmkr2ojprc53qrune76r2d27.apps.googleusercontent.com')
      }
    ]
  );
  return config;
}

const notifierDefaultOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'left',
      distance: 12
    },
    vertical: {
      position: 'bottom',
      distance: 10,
      gap: 10
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 5000,
    onClick: false,
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};

@NgModule({
  imports: [
    BrowserModule,
    NotifierModule.withConfig(
      notifierDefaultOptions
    ),
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: false}
    ),
    BrowserAnimationsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production, scope: '.'}),
    SocialLoginModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(fbConfig)
  ], providers: [
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }, AsyncPipe, MessagingService
  ],
  declarations: [
    AppComponent,
    MenuComponent,
    PageNotFoundComponent,
    HomeComponent,
    RulesComponent,
    RoomsComponent,
    NotificationsComponent,
    FriendsComponent,
    LoginComponent,
    ProfileComponent,
    RoomComponent,
    RoomCreateComponent,
    RegisterComponent,
    BoardComponent,
    VerificationComponent,
    PlayerComponent,
    VerificationComponent,
    MailComponent,
    MenuItemComponent,
    RoomItemComponent,
    NotificationItemComponent,
    ChatComponent,
    BoardMenuComponent,
    HistoryComponent,
    HistoryItemComponent,
    ResetPasswordComponent,
    ChangePasswordComponent,
    EndGameComponent
  ],
  bootstrap: [AppComponent]
})


export class AppModule {
}
