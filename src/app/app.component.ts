import {Component, OnDestroy, OnInit} from '@angular/core';
import {TileService} from './services/tile/tile.service';
import {SwPush, SwUpdate} from '@angular/service-worker';
import {NotificationService} from './services/notification/notification.service';
import {WebsocketService} from './services/websocket/websocket.service';
import {AuthenticationService} from './components/authentication/authenticationService';
import {NotifierService} from 'angular-notifier';
import {Router} from '@angular/router';
import {MessagingService} from './services/messaging/messaging.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  private notifier: NotifierService;
  rendered = false;


  constructor(private swUpdate: SwUpdate, private tileService: TileService, private swPush: SwPush,
              private notificationService: NotificationService, private websocketService: WebsocketService,
              public authenticationService: AuthenticationService, private notifierService: NotifierService,
              private router: Router, private messagingService: MessagingService) {
    this.notifier = this.notifierService;
  }


  ngOnInit(): void {
    const userId = 'user001';
    this.messagingService.requestPermission(userId);
    this.messagingService.receiveMessage();
    this.routeChanged();
  }

  routeChanged() {
    if (this.authenticationService.currentUserValue === null) {
      this.rendered = true;
    } else {
      this.websocketService.setupConnection();
      const that = this;
      this.authenticationService.getProfile().then((value) => {
        this.websocketService.getSocket().connect({}, (frame) => {
            that.websocketService.getSocket().subscribe('/user/' + value.username + '/notification', (message) => {
              that.notifier.notify('default', message.body);
            }, () => {
                setTimeout(() => {
                  this.routeChanged();
                });
            });
          that.rendered = true;
        });
      });
    }
  }

  ngOnDestroy(): void {
    if (this.websocketService.getSocket()) {
      this.websocketService.disconnect();
    }
  }
}
