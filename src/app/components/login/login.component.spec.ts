import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import {By} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {AuthService, AuthServiceConfig, SocialLoginModule} from 'angular-6-social-login-v2';
import {getAuthServiceConfigs, getMockedConfig} from '../../app.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
   declarations: [ LoginComponent ], providers: [
        {
          provide: AuthServiceConfig,
          useFactory: getMockedConfig
        }
      ], schemas:      [ NO_ERRORS_SCHEMA ],
      imports: [ FormsModule, HttpClientTestingModule, RouterTestingModule, SocialLoginModule ]
    //
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Button with login exists', () => {
      // const fixture = TestBed.createComponent(LoginComponent);
      fixture.detectChanges();
      const compiled = fixture.nativeElement.querySelectorAll('button');
      expect(compiled.item(0).textContent).toContain('Login');
      expect(compiled.item(1).textContent).toContain('Sign in with Facebook');
      expect(compiled.item(2).textContent).toContain('Sign in with Google');

  });

  it('there should be two input tags', () => {
    const fixture = TestBed.createComponent(LoginComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.queryAll(By.css('input'));
    expect(compiled.length === 2).toBeTruthy();
  });
});
