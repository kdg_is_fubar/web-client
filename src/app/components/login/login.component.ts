import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../authentication/authenticationService';
import {AuthService, FacebookLoginProvider, GoogleLoginProvider} from 'angular-6-social-login-v2';
import {UserService} from '../../services/user/user.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public username = '';
  private email;
  public password = '';
  private returnUrl: string;
  public unabletologin: boolean;

  constructor(private authenticationService: AuthenticationService,
              private router: Router,
              private socialAuthService: AuthService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.returnUrl = '/';
  }

  /**
   * Asks the authenticationService to make a call to the backend to log the user in with the current credentials.
   * This does not guarantee a successful log in.
   */
  login() {
    this.authenticationService.login(this.username, this.password).then(isLogin => {
      console.log(isLogin);
      if (isLogin) {
        this.unabletologin = false;
        window.location.reload();
        this.router.navigate([this.returnUrl]);
      } else {
        this.unabletologin = true;
      }
    })
  }

  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;

    if (socialPlatform === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform + ' sign in data : ', userData);
        this.username = userData.name;
        this.email = userData.email;
        this.password = userData.id;
        setTimeout(this.userService.registerUser(this.username, this.email, this.password).subscribe(null, error => console.log('oops', error)), 100);
      }, error => {

        console.log('ERROR:', error);

      }
    );
  }

}
