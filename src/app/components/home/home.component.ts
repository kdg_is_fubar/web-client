import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/user/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  private username: string;
  private token: string;

  constructor(private route: ActivatedRoute, private userService: UserService) {
  }

  ngOnInit() {
    this.username = this.route.snapshot.queryParamMap.get('username');
    this.token = this.route.snapshot.queryParamMap.get('token');
    if (this.username !== undefined && this.token !== undefined) {
      this.userService.verifyToken(this.username, this.token);
    }
  }

}
