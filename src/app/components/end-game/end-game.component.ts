import {Component, Input, OnInit} from '@angular/core';
import {GameService} from '../../services/game/game.service'; 
@Component({
  selector: 'app-end-game',
  templateUrl: './end-game.component.html',
  styleUrls: ['./end-game.component.scss']
})
export class EndGameComponent implements OnInit {
  @Input() roomId = 0;
  playerUsernames: string[];
  scores: number[];

  constructor(private gameService: GameService,) {
  }

  ngOnInit() {
      this.gameService.getLeaderboard(this.roomId).subscribe(leaderboard => {
        this.playerUsernames = leaderboard.playerUsernames;
        this.scores = leaderboard.scores;
      });
  }

}
