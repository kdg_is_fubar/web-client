import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../authentication/authenticationService';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../services/user/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public visitingUsername: string;
  constructor(private authenticationService: AuthenticationService,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserService) {
  }

  public email;
  public name;
  public username;
  public times_played;
  public times_won;
  public average_score;
  public maximum_score;
  public isPublic: boolean;
  public isVisitor;

  ngOnInit() {
    this.visitingUsername = this.route.snapshot.paramMap.get('username');
    this.email = 'john@doe.com';
    this.name = 'John doe';
    this.username = 'JohnDoe123';
    if (!this.visitingUsername) {
    this.authenticationService.getProfile().then(value => {
      this.email = value.email;
      this.name = value.username;  // todo change
      this.username = value.username;
      this.times_played = value.times_played;
      this.times_won = value.times_won;
      this.isPublic = value.statsPublic;
      this.isVisitor = false;
      this.average_score = value.average_score;
      this.maximum_score = value.maximum_score;
    });
    } else {
      this.authenticationService.getVisitorProfile(this.visitingUsername).then(value => {
        this.email = value.email;
        this.name = value.username;  // todo change
        this.username = value.username;
        this.isPublic = value.statsPublic;
        this.times_played = value.times_played;
        this.times_won = value.times_won;
        this.isVisitor = true;
        this.average_score = value.average_score;
        this.maximum_score = value.maximum_score;
      });
    }
  }

  logout() {
    this.authenticationService.logout();
    window.location.reload();
  }

  changeStatsAvailability() {
    this.userService.changeAvailability().subscribe((statStatus: boolean) => {
        this.isPublic = statStatus;
    });
  }

  delete() {
    this.authenticationService.delete().then(value => window.location.reload());
  }

  history() {
    this.router.navigateByUrl('/history');
  }

  redirectToChange() {
    this.authenticationService.getProfile().then(value => {
      this.router.navigateByUrl('/changePassword?email=' + value.email);
    });
  }
}
