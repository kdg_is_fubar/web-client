import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NotificationService} from '../../services/notification/notification.service';
import {NotificationDTO} from '../../model/notification-dto';
import {Router} from '@angular/router';

@Component({
  selector: 'app-notification-item',
  templateUrl: './notification-item.component.html',
  styleUrls: ['./notification-item.component.scss']
})
export class NotificationItemComponent implements OnInit {
  @Input() notification: NotificationDTO = {
    id: 0, message: '', roomId: 0, roomName: '', username: 'haha', isStarted: false
  };
  @Output() delete: EventEmitter<any> = new EventEmitter();

  constructor(private notificationService: NotificationService, private router: Router) {

  }

  ngOnInit() {

  }

  close() {
    this.notificationService.delete(this.notification.id).subscribe(
      value => this.delete.emit(this.notification.id));
  }

  redirectToBoard() {
    this.router.navigateByUrl('/board/' + this.notification.roomId);
  }

  joinRoom() {
    this.router.navigateByUrl('/room/' + this.notification.roomId);
  }
}
