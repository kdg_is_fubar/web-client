import {Component, OnDestroy, OnInit} from '@angular/core';
import {NotificationService} from '../../services/notification/notification.service';
import {NotificationDTO} from '../../model/notification-dto';
import {AuthenticationService} from '../authentication/authenticationService';
import {WebsocketService} from '../../services/websocket/websocket.service';
import {not} from 'rxjs/internal-compatibility';

class NotifyDTO {
  username: string;
  body: string;
}

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})


export class NotificationsComponent implements OnInit, OnDestroy {
  noNotifications = true;
  private socket: any;
  private userName: string;
  notifications: NotificationDTO[] = null;

  constructor(private notificationService: NotificationService, private autenticationService: AuthenticationService, private websocketService: WebsocketService) {
  }

  ngOnDestroy(): void {
  }

  ngOnInit() {
    this.pull();
  }

  pull() {
    this.notificationService.getNotifications().subscribe(value => {
      this.notifications = value;
      console.log(this.notifications);
      this.noNotifications = false;
    });
  }
}
