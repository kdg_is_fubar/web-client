import {async, ComponentFixture, ComponentFixtureAutoDetect, TestBed} from '@angular/core/testing';

import { NotificationsComponent } from './notifications.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {NotificationItemComponent} from '../notification-item/notification-item.component';
import {NotifierModule} from 'angular-notifier';
import {WebsocketService} from '../../services/websocket/websocket.service';
import {NotificationService} from '../../services/notification/notification.service';
import {AuthenticationService} from '../authentication/authenticationService';
import {BehaviorSubject} from 'rxjs';
import {NotificationDTO} from '../../model/notification-dto';
import {By} from '@angular/platform-browser';

describe('NotificationsComponent', () => {
  let component: NotificationsComponent;
  let fixture: ComponentFixture<NotificationsComponent>;
  const testText = 'some Test Text that should never appear in deployment, neither accidentally or purposefully';

  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationsComponent, NotificationItemComponent ],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true }
      ],
      imports: [HttpClientTestingModule, RouterTestingModule, NotifierModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('no Notifications', () => {
    const h2s = fixture.nativeElement.querySelectorAll('h2');
    expect(h2s.item(0).textContent).toContain('No new notifications');
  });

  it('should display Notifications', () => {
    component.notifications = [];
    component.noNotifications = false;
    const newNotification: NotificationDTO = {
      id : 0,
      message: testText,
      roomId: null,
      roomName: null,
      username: 'yellows',
      isStarted: false
    };
    component.notifications.push(newNotification);
    fixture.detectChanges();
    const items = fixture.nativeElement.querySelectorAll('app-notification-item');
    expect(items.length === 1).toBeTruthy();
    expect(items.item(0).textContent === testText + 'Close').toBeTruthy();
  });
});
