import {Component, OnInit} from '@angular/core';
import {RoomService} from '../../services/room/room.service';
import {AuthenticationService} from '../authentication/authenticationService';
import {Router} from '@angular/router';
import {TimeInterval} from 'rxjs/internal-compatibility';
import {Time} from '@angular/common';

@Component({
  selector: 'app-room-create',
  templateUrl: './room-create.component.html',
  styleUrls: ['./room-create.component.scss']
})
export class RoomCreateComponent implements OnInit {
  public maxPlayerCount = 2;
  public name: string;
  aiCount = 0;
  turnMinutes = 1;
  turnHours = 0;
  public isCreating: boolean;

  constructor(private roomService: RoomService, private authenticationService: AuthenticationService, private router: Router) {
  }

  ngOnInit() {
  }


  createRoom() {
    this.isCreating = true;
    this.authenticationService.getProfile().then(value => {
      let turnTime = 0;
      turnTime = turnTime + this.turnHours * 3600 * 1000;
      turnTime = turnTime + this.turnMinutes * 60 * 1000;
      this.roomService.createRoom(this.name, this.maxPlayerCount, value.username, this.aiCount, turnTime).then((room:any) =>
        this.router.navigate(['/room/' + room.id])
      );
    });
  }

  checkAiCount() {
    const max = this.maxPlayerCount - 1;
    if (this.aiCount > max) {
      this.aiCount = max;
    }
  }
}
