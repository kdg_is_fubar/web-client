import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {Room} from '../../model/room';

@Component({
  selector: 'app-room-item',
  templateUrl: './room-item.component.html',
  styleUrls: ['./room-item.component.scss']
})
export class RoomItemComponent implements OnInit {
  @Input() room: Room = new Room(0, 'noName', 4, 'Haha', 0);
  @Output() join: EventEmitter<Room> = new EventEmitter();

  constructor(private router: Router) {
  }

  ngOnInit() {
  }


  joinRoom() {
    this.join.emit(this.room);
  }


}
