import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FriendsComponent} from './friends.component';
import {FormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {Friend} from '../../model/friend';
import {By} from '@angular/platform-browser';

describe('FriendsComponent', () => {
  let component: FriendsComponent;
  let fixture: ComponentFixture<FriendsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FriendsComponent],
      imports: [FormsModule,HttpClientTestingModule,RouterTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('add to pending', () => {
    component.pendingList = [];
    let pendingDetected = false;
    const friend = new Friend();
    friend.id = 0;
    friend.pending = true;
    friend.username = 'test User';
    component.pendingList.push(friend);
    fixture.detectChanges();
    const pendings = fixture.nativeElement.querySelectorAll('p');
    pendings.forEach(x => {
      if (x.textContent === 'test User') {
      pendingDetected = true;
    }
    });
    expect(pendingDetected).toBeTruthy();
  });

  it('add to friendslist', () => {
    component.friendsList = [];
    let friendDetected = false;
    const friend = new Friend();
    friend.id = 0;
    friend.pending = true;
    friend.username = 'test User';
    component.friendsList.push(friend);
    fixture.detectChanges();
    const friends = fixture.nativeElement.querySelectorAll('div');
    friends.forEach(x => {
      if (x.textContent === 'test User') {
        friendDetected = true;
      }
    });
    expect(friendDetected).toBeTruthy();
  });
});
