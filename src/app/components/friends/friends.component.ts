import {Component, OnInit} from '@angular/core';
import {FriendService} from '../../services/friend/friend.service';
import {AuthenticationService} from '../authentication/authenticationService';
import {FriendDTO} from '../../model/friendDTO';
import {Friend} from '../../model/friend';
import {Router} from "@angular/router";

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit {
  // used by component
  public friendsList = [];
  public pendingList = [];

  // privates
  private currentUsername;
  public selectedFriend: string;
  public selectedInvite: string= '';
  public addUsername: string;

  constructor(private friendService: FriendService, private authenticationService: AuthenticationService, private router: Router) {
  }

  ngOnInit() {
    this.authenticationService.getProfile().then(value => {
      this.currentUsername = value.username;
      this.getFriends();
    });
  }

  getFriends() {
    this.friendService.getFriends().subscribe(data => this.orderData(data));
  }

  private orderData(data) {
    this.pendingList = [];
    this.friendsList = [];
    const friendData = data.filter(x => x.pending === false);
    const pendingData = data.filter(x => x.pending === true);

    for (const pending of pendingData) {
      const convertedPending = new Friend();
      convertedPending.id = pending.id;
      convertedPending.username = pending.username;
      convertedPending.pending = pending.pending;
      this.pendingList.push(convertedPending);
    }

    for (const friend of friendData) {
      const convertedFriend = new Friend();
      convertedFriend.id = friend.id;
      convertedFriend.username = friend.username;
      convertedFriend.pending = friend.pending;
      this.friendsList.push(convertedFriend);

    }
  }

  addFriend() {
    const friendDTO: FriendDTO = new FriendDTO();
    friendDTO.currentUsername = this.currentUsername;
    friendDTO.friendUsername = this.addUsername;
    this.friendService.addFriend(friendDTO).subscribe(data => {
      this.orderData(data);
    });

    this.selectedInvite = '';
    this.currentUsername = '';
    this.addUsername = '';

  }

  deleteFriend(friend) {
    const friendDTO: FriendDTO = new FriendDTO();
    friendDTO.currentUsername = this.currentUsername;
    friendDTO.friendUsername = friend.username;
    this.friendService.deleteFriend(friendDTO).subscribe(data => {
      this.orderData(data);
    });
  }

  acceptInvite() {
    const friendDTO: FriendDTO = new FriendDTO();
    friendDTO.currentUsername = this.currentUsername;
    friendDTO.friendUsername = this.selectedInvite;
    this.friendService.acceptInvite(friendDTO).subscribe(data => {
      this.orderData(data);
    });
    this.selectedInvite = '';
  }

  refuseInvite() {
    const friendDTO: FriendDTO = new FriendDTO();
    friendDTO.currentUsername = this.currentUsername;
    friendDTO.friendUsername = this.selectedInvite;
    this.friendService.refuseInvite(friendDTO).subscribe(data => {
      this.orderData(data);
    });
    this.selectedInvite = '';
  }

  selectFriend(childItem) {
    this.selectedFriend = childItem.username;
    console.log(this.selectedFriend);

  }

  selectInvite(childItem) {
    this.selectedInvite = childItem.username;
    console.log(this.selectedInvite);
  }

  isSelectedFriend(pending: any) {
    return pending.username == this.selectedFriend;
  }

  isSelectedInvite(pending: any) {
    return pending.username == this.selectedInvite;
  }

  moveToProfile(username: any) {
    this.router.navigateByUrl('/profile/' + username);

  }
}
