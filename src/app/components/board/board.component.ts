import {AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TileService} from '../../services/tile/tile.service';
import {RoomService} from '../../services/room/room.service';
import {AuthenticationService} from '../authentication/authenticationService';
import {CanvasService} from '../../services/canvas/canvas.service';
import {ActivatedRoute, Router} from '@angular/router';
import {WebsocketService} from '../../services/websocket/websocket.service';
import {BoardStatus} from '../../model/board-status.enum';
import {HistoryService} from '../../services/history/history.service';
import {GameService} from '../../services/game/game.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
// TODO CLEAN THE FUCK UP!!!!!!!!!!
export class BoardComponent implements OnInit, AfterViewInit, OnDestroy {

  canvasService: CanvasService;
  roomService: RoomService;
  @ViewChild('board_canvas') canvas: ElementRef;
  private timeoutHandler;
  private zoom = 150;
  private readonly zoomFactor = 5;
  private readonly minZoom = 70;
  private readonly maxZoom = 200;
  private mouseDown: boolean;
  private bufferedEvent: MouseEvent;
  private _canvas;
  private ctx;
  private websocket;
  private subGameUrl;
  rendered = false;
  roomid: number;
  private username;
  replayId: number;
  public players;
  public show_players = false;
  private subGame: any;
  private subHand: any;
  activatedRoute;

  constructor(private authenticationService: AuthenticationService,
              private _canvasService: CanvasService,
              private websocketService: WebsocketService,
              private route: ActivatedRoute,
              private router: Router,
              private _roomService: RoomService,
              private historyService: HistoryService,
              private gameService: GameService) {
    this.canvasService = _canvasService;
    this.roomService = _roomService;
    this.activatedRoute = route;
  }

  ngOnInit(): void {
    if (this.router.url.includes('replay')) {
      this.replayId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
      console.log("replay" + this.replayId);
      this.canvasService.boardStatus = BoardStatus.REPLAY;
      this.roomid = null;
    } else {
      this.roomid = parseInt(this.route.snapshot.paramMap.get('id'), 10);
      this.replayId = null;
      console.log("roomid" + this.roomid);

    }
  }

  ngOnDestroy(): void {
    if (this.subGame !== undefined || this.subHand !== undefined) {
      this.websocketService.getSocket().unsubscribe(this.subGame);
      this.websocketService.getSocket().unsubscribe(this.subHand);
    }
  }

  processVerification(boolean) {
    this.canvasService.verifying = false;
    if (boolean) {
      this.canvasService.placeFollower(this.canvasService.tileX, this.canvasService.tileY, this.canvasService.partX, this.canvasService.partY);
      this.canvasService.boardStatus = BoardStatus.AWAITING_TURN;
    }
  }


  initWebsockets() {
    if (this.websocketService.getSocket()) {
      if (this.websocketService.getSocket().connected) {
        this.websocket = this.websocketService.getSocket();
          this.subGameUrl = '/game/' + this.roomid;
        this.subGame = this.websocket.subscribe(this.subGameUrl, (gameState) => {
          // update board

          const game = JSON.parse(gameState.body);
          console.log(game);
          this.canvasService.ended = game.hasEnded;
          this.checkBoardState(game.username);
          this.canvasService.updateTiles(game.tiles);
          this.players = game.players;
        });

        this.subHand = this.websocketService.getSocket().subscribe('/user/' + this.username + '/hand', (message) => {
          this.canvasService.boardStatus = BoardStatus.PLACING_TILE;
          this.canvasService.placeTileInHand(JSON.parse(message.body));
        });
        this.rendered = true;
        // });
      }
    }
  }

  checkBoardState(username: string) {
    if (this.canvasService.boardStatus !== BoardStatus.REPLAY) {
      console.log('turn : ' + username);
      if (username === this.username && this.canvasService.boardStatus === BoardStatus.AWAITING_TURN) {
        this.canvasService.boardStatus = BoardStatus.PLACING_TILE;
      } else if (username !== this.username) {
        this.canvasService.boardStatus = BoardStatus.AWAITING_TURN;
        this.canvasService.toPlaceTile = undefined;
      }
    }
  }

  ngAfterViewInit() {
    if (this.canvasService.boardStatus === BoardStatus.REPLAY)
      this.updateReplayBoard();
    else{
      this.authenticationService.getProfile().then(value => {
        this.username = value.username;
        this.canvasService.boardStatus = BoardStatus.AWAITING_TURN;
        this.canvasService.initGame(this.roomid, value.username);
        this.initWebsockets();
      });
    }
    const canvas = this.canvas.nativeElement;
    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
    this._canvas = canvas;
    this.ctx = canvas.getContext('2d');
    this.canvasService.centerX = canvas.width / 2;
    this.canvasService.centerY = canvas.height / 2;
    this.canvasService.canvas = this._canvas;
    this.canvasService.ctx = this.ctx;
  }

  updateReplayBoard() {
    if (this.canvasService.boardStatus === BoardStatus.REPLAY) {
      this.historyService.getNext(this.replayId).subscribe(value => this.canvasService.updateTiles(value));
    }
  }

  replayReset() {
    this.historyService.reset(this.replayId).subscribe(value => this.canvasService.updateTiles(value));
  }

  //#region mouse

  @HostListener('mousewheel', ['$event'])
  @HostListener('mousedown')
  private onScroll(event) {
    let zoomChanged = false;
    if (event.wheelDelta > 0) {
      if (this.canvasService.zoom < this.maxZoom) {
        this.canvasService.zoom += this.zoomFactor;
        zoomChanged = true;
      }
    } else {
      if (this.canvasService.zoom > this.minZoom) {
        this.canvasService.zoom -= this.zoomFactor;
        zoomChanged = true;
      }
    }
    if (zoomChanged) {
      this.canvasService.redrawCanvas();
    }
  }

  @HostListener('mousedown', ['$event'])
  private onMouseDown(event) {
    if (event.path[0].id === 'board_canvas') {
      this.mouseDown = true;
      this.bufferedEvent = event;
      this.canvasService.holdingTile = (this.canvasService.toPlaceTile !== undefined &&
        ((event.layerX <= (this.canvasService.toPlaceTile.x + this.zoom)) &&
          (event.layerX >= this.canvasService.toPlaceTile.x)) &&
        ((event.layerY <= (this.canvasService.toPlaceTile.y + this.zoom)) &&
          (event.layerY >= this.canvasService.toPlaceTile.y)));
    }
  }

  @HostListener('mouseup', ['$event'])
  private onMouseUp(event) {
    this.mouseDown = false;
    if (this.canvasService.toPlaceTile && this.canvasService.holdingTile) {
      const x = Math.round((event.layerX - this.canvasService.centerX) / this.canvasService.zoom);
      const y = Math.round((event.layerY - this.canvasService.centerY) / this.canvasService.zoom);
      this.canvasService.toPlaceTile.x = this._canvas.width / 2 - this.canvasService.zoom / 2;
      this.canvasService.toPlaceTile.y = this._canvas.height - this.canvasService.zoom * 2;
      //
      if (!this.canvasService.toPlaceTile.isPlaced) {
        this.gameService.requestTilePlacement(parseInt(this.route.snapshot.paramMap.get('id')), this.canvasService.toPlaceTile, x, y).then(value => {
          if (value) {
            this.canvasService.putDownTile(x, y);
          } else {
            this.canvasService.redrawCanvas();
          }
        });
      }
    } else if (this.canvasService.holdingMeeple) {
      this.canvasService.tryPlaceMeeple(event);
    }
  }

  @HostListener('mousemove', ['$event'])
  private onMouseMove(event) {

    this.canvasService.cursorX = event.clientX;
    this.canvasService.cursorY = event.clientY;
    if (this.mouseDown && !this.canvasService.holdingTile) {
      this.canvasService.centerX += event.clientX - this.bufferedEvent.clientX;
      this.canvasService.centerY += event.clientY - this.bufferedEvent.clientY;
      this.bufferedEvent = event;
      this.canvasService.redrawCanvas();
    } else if (this.mouseDown && this.canvasService.holdingTile) {
      this.canvasService.toPlaceTile.x += event.clientX - this.bufferedEvent.clientX;
      this.canvasService.toPlaceTile.y += event.clientY - this.bufferedEvent.clientY;
      this.bufferedEvent = event;
      this.canvasService.redrawCanvas();
      this.canvasService.lastplaceX = this.canvasService.getMousePos(this._canvas, event).x;
      this.canvasService.lastplaceY = this.canvasService.getMousePos(this._canvas, event).y;
    } else if (this.canvasService.holdingMeeple) {
      this.canvasService.redrawCanvas();
    }
  }

  public zoomInMousedown() {
    this.timeoutHandler = setInterval(() => {
      this.zoomIn();
    }, 50);
  }

  public zoomOutMousedown() {
    this.timeoutHandler = setInterval(() => {
      this.zoomOut();
    }, 50);
  }

  //#endregion

  //#region touch
  @HostListener('touchstart', ['$event'])
  private onTouchDown(e) {
    const touch = e.touches[0];
    if (touch.target.id === 'zoom_in') {
      this.zoomInMousedown();
    } else if (touch.target.id === 'zoom_out') {
      this.zoomOutMousedown();
    } else {
      this.onMouseDown({
        path: {0: {id: touch.target.id}},
        clientX: touch.clientX,
        clientY: touch.clientY,
        layerX: touch.pageX - this._canvas.offsetLeft,
        layerY: touch.pageY - this._canvas.offsetTop
      });
    }
  }


  @HostListener('touchend', ['$event'])
  private onTouchEnd(e) {
    const touch = e.changedTouches[0];
    this.resetInterval();
    this.onMouseUp({
      path: {0: {id: touch.target.id}},
      clientX: touch.clientX,
      clientY: touch.clientY,
      layerX: touch.pageX - this._canvas.offsetLeft,
      layerY: touch.pageY - this._canvas.offsetTop
    });

  }

  @HostListener('touchmove', ['$event'])
  private onTouchMove(e) {
    const touch = e.touches[0];
    this.onMouseMove({
      path: {0: {id: touch.target.id}},
      clientX: touch.clientX,
      clientY: touch.clientY,
      layerX: touch.pageX - this._canvas.offsetLeft,
      layerY: touch.pageY - this._canvas.offsetTop
    });
  }


//#endregion

  zoomIn() {
    if (this.canvasService.zoom < this.maxZoom) {
      this.canvasService.zoom += this.zoomFactor * 2;
      this.canvasService.redrawCanvas();
    }
  }

  zoomOut() {
    if (this.canvasService.zoom > this.minZoom) {
      this.canvasService.zoom -= this.zoomFactor * 2;
      this.canvasService.redrawCanvas();
    }
  }

  public resetInterval() {
    if (this.timeoutHandler) {
      clearInterval(this.timeoutHandler);
      this.timeoutHandler = null;
    }
  }

  public endTurn() {
    console.log('ENDED TURN');
    this.gameService.endTurn(this.roomid);
    this.canvasService.endTurn();
  }
}
