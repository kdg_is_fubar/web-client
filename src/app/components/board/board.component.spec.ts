import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BoardComponent} from './board.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {ChatComponent} from '../chat/chat.component';
import {FormsModule} from '@angular/forms';
import {BoardMenuComponent} from '../board-menu/board-menu.component';
import {NotifierModule} from 'angular-notifier';
import {EndGameComponent} from '../end-game/end-game.component';

describe('BoardComponent', () => {
  let component: BoardComponent;
  let fixture: ComponentFixture<BoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardComponent, ChatComponent, BoardMenuComponent, EndGameComponent ],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule, NotifierModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
