import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {BoardMenuComponent} from './board-menu.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {BoardStatus} from '../../model/board-status.enum';
import {By} from '@angular/platform-browser';

describe('BoardMenuComponent', () => {
  let component: BoardMenuComponent;
  let fixture: ComponentFixture<BoardMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BoardMenuComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change divs depending on the boardstatus', () => {
    component.boardStatus = BoardStatus.AWAITING_TURN;
    fixture.detectChanges();
    let divs = fixture.debugElement.queryAll(By.css('div'));
    expect(divs.length === 1).toBeTruthy();

    component.boardStatus = BoardStatus.REPLAY;
    fixture.detectChanges();
    divs = fixture.debugElement.queryAll(By.css('div'));
    expect(divs.length === 3).toBeTruthy();

    component.boardStatus = BoardStatus.PLACING_TILE;
    fixture.detectChanges();
    divs = fixture.debugElement.queryAll(By.css('div'));
    expect(divs.length === 1).toBeTruthy();

    component.boardStatus = BoardStatus.PLACED_TILE;
    fixture.detectChanges();
    divs = fixture.debugElement.queryAll(By.css('div'));
    expect(divs.length === 2).toBeTruthy();

    component.boardStatus = BoardStatus.PLACING_FOLLOWER;
    fixture.detectChanges();
    divs = fixture.debugElement.queryAll(By.css('div'));
    expect(divs.length === 1).toBeTruthy();
  });
});
