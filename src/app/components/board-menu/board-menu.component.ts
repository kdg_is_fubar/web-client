import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BoardStatus} from '../../model/board-status.enum';

@Component({
  selector: 'app-board-menu',
  templateUrl: './board-menu.component.html',
  styleUrls: ['./board-menu.component.scss']
})
export class BoardMenuComponent implements OnInit {
  @Input() public boardStatus: BoardStatus;
  @Output() pickedOrRotatedTile: EventEmitter<void> = new EventEmitter<void>();
  @Output() pickedFollower: EventEmitter<void> = new EventEmitter<void>();
  @Output() endedTurn: EventEmitter<void> = new EventEmitter<void>();
  @Output() canceledFollower: EventEmitter<void> = new EventEmitter<void>();
  @Output() replayNextTurn: EventEmitter<void> = new EventEmitter<void>();
  @Output() replayReset: EventEmitter<void> = new EventEmitter<void>();
  public BoardStatus = BoardStatus;

  ngOnInit() {

  }
}
