import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import {MatButtonModule, MatFormFieldModule, MatInputModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserTestingModule} from '@angular/platform-browser/testing';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterComponent ],
      imports: [MatButtonModule, MatInputModule, MatFormFieldModule, FormsModule, HttpClientTestingModule, RouterTestingModule, BrowserTestingModule]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display password error', () => {
    component.username = 'test User';
    component.email = 'someEmail';
    component.password = 'FirstPassword';
    component.verifyPass = 'notTheSame';
    fixture.detectChanges();
    component.register();
    expect(component.passwordError).toBeTruthy();
  });

  it('should display input error', () => {
    component.username = '';
    component.email = 'someEmail';
    component.password = 'FirstPassword';
    component.verifyPass = 'notTheSame';
    fixture.detectChanges();
    component.register();
    expect(component.inputError).toBeTruthy();
  });
});
