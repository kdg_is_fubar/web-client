import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../authentication/authenticationService';
import {UserService} from '../../services/user/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {


  public username = '';
  public email = '';
  public password = '';
  public verifyPass = '';
  private returnUrl: string;
  public passwordError;
  public alreadyRegistered;
  public inputError;

  constructor(private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {
    this.returnUrl = '/mail';
  }

  /**
   * Asks the authenticationService to make a call to the backend to log the user in with the current credentials.
   * This does not guarantee a successful log in.
   * @constructor
   */
  register() {
    if (this.fieldsNotEmpty()) {
      this.inputError = false;
      if (this.verifyPass === this.password) {
        this.passwordError = false;
        this.userService.registerUser(this.username, this.email, this.password).subscribe(
          (resp: Response) => {
            console.log(resp);
            return this.router.navigate([this.returnUrl]);
          },
          (error) => {
            console.log(error);
            this.alreadyRegistered = true;
          }
        );
      } else {
        this.passwordError = true;
      }
    } else {
      this.inputError = true;
    }

  }

  fieldsNotEmpty(): boolean {
    return (this.username.length > 0) && (this.password.length > 0) && (this.email.length > 4);
  }
}
