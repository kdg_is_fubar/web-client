import {Component, Input, NgModule, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent implements OnInit {

  @Input() title;
  @Input() link;
  @Input() asset;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

}
