import {Component, Input, OnInit} from '@angular/core';
import {Replay} from "../../model/replay";
import {HistoryService} from "../../services/history/history.service";

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  public replays: Replay[];
  public noReplays: Boolean;
  public isLoading = true;

  constructor(private historyService: HistoryService) {
  }

  ngOnInit() {
    this.historyService.getReplays().subscribe(replay => {
      if (replay) {
        this.noReplays = replay.length <= 0;
      }

      this.replays = replay;
      this.isLoading = false;
    });
  }

}
