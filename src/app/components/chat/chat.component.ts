import {AfterViewChecked, Component, ElementRef, Input, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {WebsocketService} from '../../services/websocket/websocket.service';
import {AuthenticationService} from '../authentication/authenticationService';

class ChatDTO {
  id: number;
  from: string;
  body: string;
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, AfterViewChecked, OnDestroy {
  @Input() roomid;

  currentUser;
  chatHistory = [];
  currentMessage;
  public showChat = false;

  @ViewChild('scroll') private myScrollContainer: ElementRef;
  @ViewChild('input') private inputField: ElementRef;
  private subChat: any;

  constructor(private websocketService: WebsocketService, private authenticationService: AuthenticationService) {

  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

  ngOnDestroy(): void {
    if (this.subChat !== undefined) {
    this.websocketService.getSocket().unsubscribe(this.subChat);
    }
  }

  ngOnInit() {
    this.initWebsockets();
    this.scrollToBottom();
  }

  initWebsockets() {
    if (this.websocketService.getSocket()) {
      if (this.websocketService.getSocket().connected) {
        this.subChat = this.websocketService.getSocket().subscribe('/chat/' + this.roomid, (message) => {
          console.log(message.body);
          if (this.chatHistory.length >= 20) {
            this.chatHistory.splice(0, 1);
          }
          this.chatHistory.push(message.body);
        // });


      });
      }
    }
      this.authenticationService.getProfile().then( userInfo => {
      this.currentUser = userInfo.username;
    });

  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  sendMessage() {
    if (this.currentMessage !== '') {
      const chatDTO: ChatDTO = new ChatDTO();
      chatDTO.from = this.currentUser;
      chatDTO.body = this.currentMessage;
      chatDTO.id = this.roomid;
      this.websocketService.getSocket().send('/rooms/chat/' + this.roomid, {}, JSON.stringify(chatDTO));
      this.currentMessage = '';
      this.inputField.nativeElement.focus();
    }
  }
}
