import {Component, OnDestroy, OnInit} from '@angular/core';
import {Room} from '../../model/room';
import {RoomService} from '../../services/room/room.service';
import {Router} from '@angular/router';
import {AuthenticationService} from '../authentication/authenticationService';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit, OnDestroy {
  public rooms: Room[];
  public unfilteredRooms: Room[];
  public isLoading = true;
  private refreshTimeout;
  public filter ;
  private username;
  public filterList = ['All', 'Free', 'Joined', 'My Turn'];

  constructor(private roomService: RoomService, private router: Router, private authenticationService: AuthenticationService) {
    this.filter = this.filterList[0];
  }

  ngOnInit() {
    this.authenticationService.getProfile().then(value => {
      this.username = value.username;
    });
    // TODO change by Observable timeout
    this.refreshTimeout = setInterval(() => {
      this.roomService.getRooms().subscribe(value => {
        this.unfilteredRooms = value;
        this.isLoading = false;
        if (!this.rooms) {
          this.rooms = this.unfilteredRooms;
        } else {
          this.filterRoom();
        }
      });
    }, 1000);
  }


  joinRoom(room: Room) {
    this.router.navigateByUrl('/room/' + room.id);
  }

  ngOnDestroy(): void {
    clearTimeout(this.refreshTimeout);

  }

  filterRoom() {
    this.rooms = this.unfilteredRooms;
    if (this.filter === undefined || this.filter === 'All') {
      this.rooms = this.unfilteredRooms;
    } else if (this.filter === 'Free') {
      this.rooms = this.rooms.filter(room => room.players.length < room.maxPlayerCount);
    } else if (this.filter === 'Joined') {
      this.rooms = this.rooms.filter(room => room.players.filter(player => player.username == this.username).length === 1);
    } else if (this.filter === 'My Turn') {
      this.rooms = this.rooms.filter(room => room.players[room.currentPlayerId].username == this.username);
    }
  }
}
