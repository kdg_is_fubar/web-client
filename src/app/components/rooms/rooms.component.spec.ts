import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomsComponent } from './rooms.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {RoomItemComponent} from '../room-item/room-item.component';
import {Room} from '../../model/room';
import {FormsModule} from '@angular/forms';

describe('RoomsComponent', () => {
  let component: RoomsComponent;
  let fixture: ComponentFixture<RoomsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomsComponent, RoomItemComponent ],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display no room', () => {
    component.isLoading = false;
    component.rooms = [];
    fixture.detectChanges();
    const h2s = fixture.nativeElement.querySelectorAll('h2');
    expect(h2s.length === 1).toBeTruthy();
  });

  it('should display a room', () => {
    component.isLoading = false;
    fixture.detectChanges();
    component.rooms = [];
    const room = new Room(0, 'test', 5, 'test User', 0);
    room.players = ['appelmoes'];
    component.rooms.push(room);
    fixture.detectChanges();
    const rooms = fixture.nativeElement.querySelectorAll('app-room-item');
    expect(rooms.length === 1).toBeTruthy();
  });
});
