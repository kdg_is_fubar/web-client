import {Component, HostListener, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {WebsocketService} from '../../services/websocket/websocket.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  menuText: string;
  isMenuClicked: boolean;
  isLoggedIn: boolean;


  constructor(private router: Router, private websocketService: WebsocketService) {
    router.events.subscribe((val) => {
      this.updateMenuText();
    }, error => console.log('oops', error));


  }

  ngOnInit() {
    this.isLoggedIn = !(localStorage.getItem('userToken') === null);
    this.isMenuClicked = false;
  }

  updateMenuText() {
    if (this.router.url.indexOf('?') >= 1) {
      this.menuText = this.router.url.charAt(1).toUpperCase() + this.router.url.substr(2, this.router.url.indexOf('?') - 2).toLowerCase().replace('_', ' ');

    } else {
      this.menuText = this.router.url.charAt(1).toUpperCase() + this.router.url.substr(2).toLowerCase().replace('_', ' ');

    }
    if (this.menuText === '') {
      this.menuText = 'Home';
    }
    this.isMenuClicked = false;
  }


  @HostListener('touchstart', ['$event'])
  touchstart_menu(event) {
    if (event.path[0].id === 'menu_background') {
      this.isMenuClicked = false;
    }
  }
}

