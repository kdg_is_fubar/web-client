import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuComponent } from './menu.component';
import {RouterTestingModule} from '@angular/router/testing';
import {MenuItemComponent} from '../menu-item/menu-item.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NotifierModule} from 'angular-notifier';
import {By} from '@angular/platform-browser';

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuComponent , MenuItemComponent],
      imports: [RouterTestingModule, HttpClientModule, NotifierModule, RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have 4 menu items', () => {
    const allListItems = fixture.debugElement.queryAll(By.css('app-menu-item'));
    expect(allListItems.length === 4).toBeTruthy();
  });
});
