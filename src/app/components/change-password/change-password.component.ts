import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user/user.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  private email: string;
  pass: string;
  secondPass: string;

  constructor(private userService: UserService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.email = this.route.snapshot.queryParamMap.get('email');
  }

  changePassword() {
    if (this.pass === this.secondPass){
      console.log(this.email);
      console.log(this.pass);
      this.userService.changePassword(this.email, this.pass).subscribe();
    }
  }
}
