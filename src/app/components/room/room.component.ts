import {AfterContentInit, AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RoomService} from '../../services/room/room.service';

import {JoinRoomDTO} from '../../model/join-room-dto';
import {AuthenticationService} from '../authentication/authenticationService';
import {WebsocketService} from '../../services/websocket/websocket.service';
import {FriendService} from '../../services/friend/friend.service';
import {Friend} from '../../model/friend';
import {InviteStatus} from '../../model/InviteStatus';
import {InvitationService} from '../../services/invitation/invitation.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit, OnDestroy, AfterViewInit {

  room;
  roomId: number;
  private subJoinUrl;
  private subbedJoin;
  private subbedGame;
  gameStarted = false;
  rendered = false;

  public currentcount = 0;
  friendsList = [];
  private subGameUrl: string;
  public isJoining = false;
  public friendsToggle = false;
  public invited = false;
  public isFull = false;
  hasResponse = false;
  response: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private roomService: RoomService,
              private invitationService: InvitationService,
              private websocketService: WebsocketService, private authenticationService: AuthenticationService,
              private friendService: FriendService) {
  }

  initWebsockets() {
    console.log(this.room);
    this.subJoinUrl = '/joined/' + this.roomId;
    this.subGameUrl = '/game/' + this.roomId;
    if (this.websocketService.getSocket() && this.websocketService.getSocket().connected) {
      this.authenticationService.getProfile().then(value => {
        this.websocketService.joinRoom(this.room, value.username);
      });
      this.subbedJoin = this.websocketService.getSocket().subscribe(this.subJoinUrl, (message) => {
        const joiningObject = JSON.parse(message.body);
        if (joiningObject.joining === true) {
          this.room.players.push(joiningObject.joiningPlayer);
          this.currentcount = this.room.players.length;
        } else if (!joiningObject.joining) {
          this.room.players.splice(this.room.players.findIndex(x => x.username === joiningObject.joiningPlayer.username), 1);
          this.currentcount = this.room.players.length;
        }
        this.isFull = this.currentcount === this.room.maxPlayerCount;
        console.log(this.isFull);

      });

      this.subbedGame = this.websocketService.getSocket().subscribe(this.subGameUrl, (message) => {
        this.router.navigateByUrl('/board/' + this.roomId);
      });
      this.isFull = this.currentcount === this.room.maxPlayerCount;
      this.rendered = true;
    }

  }

  ngOnInit() {
    this.roomId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.room = {
      name: '',
      maxPlayerCount: 0,
      username: '',
      aiCount: 0,
      currentPlayerId: 0,
      players: [],
    };

  }

  getFriends() {
    this.friendService.getFriends().subscribe(data => this.convertToFriend(data));
  }

  convertToFriend(data) {
    this.friendsList = [];
    const friendData = data.filter(x => x.pending === false);
    for (const friend of friendData) {
      const convertedFriend = new Friend();
      convertedFriend.id = friend.id;
      convertedFriend.username = friend.username;
      convertedFriend.pending = friend.pending;
      this.friendsList.push(convertedFriend);
    }
  }

  ngOnDestroy(): void {
    this.websocketService.getSocket().unsubscribe(this.subbedJoin);
    this.websocketService.getSocket().unsubscribe(this.subbedGame);
  }

  watchGame() {
    this.isJoining = true;
    this.router.navigateByUrl('/board/' + this.roomId);
  }

  startGame() {
    this.isJoining = true;
    if (!this.gameStarted) {
      this.authenticationService.getProfile().then(value => {
        this.websocketService.getSocket().send('/rooms/game/' + this.roomId + '/start', {}, value.username);
      });
    } else {
      this.router.navigateByUrl('/board/' + this.roomId);
    }
  }


  leaveRoom() {
    this.isJoining = true;
    this.authenticationService.getProfile().then(value => {
      const joinroom = new JoinRoomDTO();
      joinroom.id = this.roomId;
      joinroom.username = value.username;
      joinroom.joining = false;
      this.websocketService.getSocket().send('/rooms/join/' + this.roomId, this.authenticationService.getOptions(), JSON.stringify(joinroom));
    });

  }

  inviteFriend(friend: Friend) {
    this.invitationService.inviteRoom(this.room, friend.username).subscribe(
      value => this.invited = true
    )
    ;
  }

  invite(username: string) {
    this.invitationService.inviteRoom(this.room, username).subscribe(
      value => {
        this.hasResponse = true;
        if (value === InviteStatus.INVITATION_SENT) {
          this.response = 'An invitation has been sent to ' + username;
        } else if (value === InviteStatus.NOT_REGISTERED) {
          this.response = 'It seems like ' + username + ' is not yet known on this platform.';
        } else {
          this.response = 'A problem has occurred';
        }
      }
    );
  }

  inviteByEmail(email: string) {
    this.invitationService.inviteEmailRoom(this.room, email).subscribe(
      value => {
        this.hasResponse = true;
        if (value === InviteStatus.INVITATION_SENT) {
          this.response = 'An invitation has been sent to ' + email;
        } else if (value === InviteStatus.NOT_REGISTERED) {
          this.response = 'It seems like ' + email + ' is not yet known on this platform, we sent a mail inviting him/her to join.';
        } else {
          this.response = 'A problem has occurred';
        }
      }
    );
  }

  isPlayerInRoom(username: String) {
    let contains = false;
    if (this.room.players) {
      for (const player  of this.room.players) {
        if (player.username === username) {
          contains = true;
        }
      }
    }
    return contains;
  }

  ngAfterViewInit(): void {
    this.roomService.getRoom(this.roomId).subscribe(data => {
      this.room = data;
      console.log(data);
      this.gameStarted = data.gameStarted;
      this.currentcount = this.room.players.length;
      this.initWebsockets();
    });
    this.getFriends();
  }

  getCurrentPlayer() {
    return this.room.players[this.room.indexCurrentPlayer].username;
  }
}
