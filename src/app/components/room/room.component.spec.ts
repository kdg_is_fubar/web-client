import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RoomComponent} from './room.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {PlayerComponent} from '../player/player.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ChatComponent} from '../chat/chat.component';
import {FormsModule} from '@angular/forms';
import {NotifierModule} from "angular-notifier";

describe('RoomComponent', () => {
  let component: RoomComponent;
  let fixture: ComponentFixture<RoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RoomComponent, PlayerComponent, ChatComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule, NotifierModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
