import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {

  @Input() player;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  moveToProfile() {
    this.router.navigateByUrl('/profile/' + this.player);
  }
}
