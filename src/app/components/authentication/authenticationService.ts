import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {UserService} from '../../services/user/user.service';
import {carcassonnePlayer} from '../../model/carcassonnePlayer';
import {HttpHeaders} from '@angular/common/http';


/**
 * This class will log in/out the user and initialize an observer on a userSubject, this subject is a localstorage variable.
 * The use of this observable is for when a token gets deleted or when it's filled in, the app can always ask for the value, this will be a snapshot of the current situation.
 *
 */

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<String>;
  public currentUser: Observable<String>;
  private askedUser;

  constructor(private userService: UserService) {
    this.currentUserSubject = new BehaviorSubject<String>(localStorage.getItem('userToken'));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public getOptions() {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('userToken')
    });
  }

  public get currentUserValue(): String {
    // Get current value inside of "userToken"
    return this.currentUserSubject.value;
  }

  login(username: string, password: string): Promise<boolean> {

    return new Promise((resolve) => {
      this.userService.getUser(username, password).subscribe(
        (res) => {
          this.askedUser = res.headers.get('Authorization');
          localStorage.setItem('userToken', res.headers.get('Authorization'));
          this.currentUserSubject.next(this.askedUser);
          resolve(true);
        }, () => {
          resolve(false);
        });
    });

  }


  getProfile(): Promise<carcassonnePlayer> {
    return this.userService.getProfile();
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('userToken');
    this.currentUserSubject.next(null);
  }

  delete() {
    localStorage.removeItem('userToken');
    this.currentUserSubject.next(null);
    return this.userService.deleteUser();
  }

  getVisitorProfile(username): Promise<carcassonnePlayer> {
    return this.userService.getVisitorProfile(username);

  }

}
