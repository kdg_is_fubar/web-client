import {Component, Input, OnInit} from '@angular/core';
import {Replay} from "../../model/replay";

@Component({
  selector: 'app-history-item',
  templateUrl: './history-item.component.html',
  styleUrls: ['./history-item.component.scss']
})
export class HistoryItemComponent implements OnInit {
  @Input() public replay: Replay = new Replay();
  constructor() { }

  ngOnInit() {
  }

}
